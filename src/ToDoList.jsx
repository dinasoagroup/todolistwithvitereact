import { useEffect, useState, useRef } from "react";

function ToDoList(){
    const initial = (localStorage.getItem("todos") == null)?[]:JSON.parse(localStorage.getItem("todos"));
    const [todos, setTodos] = useState(initial);

    function handleToogleToDo(todo){
        const updatedTodos = todos.map((t) => 
            (t.id === todo.id) ?
            {
                ...t,
                "done": !t.done
            }
            : t
        );
        localStorage.setItem("todos", JSON.stringify(updatedTodos));
        setTodos(updatedTodos);
    }

    if(!todos.length){
        return (
            <>
                <h4>No task to do !</h4>
                <AddToDo setTodos={setTodos} />
            </>
        )
    }

    return (
        <>
            <h2><u>To do</u></h2>
            <ul>
            {todos.map( todo => (
                <div key={todo.id}>
                    <input type="checkbox" id={"task" + todo.id} name={"task" + todo.id} defaultChecked={todo.done} onClick={() => handleToogleToDo(todo)} />
                    <label htmlFor={"task" + todo.id}>{todo.task}</label>
                    <DeleteToDo targetedTodo={todo} setTodos={setTodos}/>
                </div>
            ))}
            </ul>
            <AddToDo setTodos={setTodos} />
        </>
    )
}

function AddToDo({setTodos}){

    const newTodoRef = useRef();

    function handleAddToDo(event){
        event.preventDefault();
        let task = event.target.elements.newTodo.value;
        setTodos((previousTodos) => {
            const newTodo = previousTodos.concat(
                {
                    "id" : previousTodos.length,
                    "task" : task,
                    "done" : false
                });
                localStorage.setItem("todos", JSON.stringify(newTodo));
            return newTodo;
        });
        newTodoRef.current.value = "";
    }

    return (
        <form onSubmit={handleAddToDo}>
            <input type="text" name="newTodo" placeholder="write your task here" ref={newTodoRef} />
            <button type="submit">Add task</button>
        </form>
    )
}

function DeleteToDo({targetedTodo, setTodos}){

    function handleDeleteTodo(){
        const deletedTodo = targetedTodo.task;
        const confirm = window.confirm("Do you really want to delete "+ deletedTodo + " ?");
        if(confirm){
            setTodos((previousTodos) => {
                const newTodo = previousTodos.filter((t) => (t.id !== targetedTodo.id));
                localStorage.setItem("todos", JSON.stringify(newTodo));
                return newTodo;
            });

            alert(deletedTodo + " deleted !");
        }
    }

    return (
        <span role="button" onClick={handleDeleteTodo}
        style={{
            color:"red",
            fontWeight:"bold",
            marginLeft: 10,
            cursor:"pointer",
        }}>X</span>
    )
}


export default ToDoList