import './App.css'
import Welcome from './Welcome';
import Clock from './Clock';
import ToDoList from './ToDoList';


function App() {
  return (
    <div>
      <Welcome name="Dinasoa" />
      <Clock />
      <ToDoList />
    </div>
  )
}


export default App
