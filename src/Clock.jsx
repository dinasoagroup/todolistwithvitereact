import React, { useEffect, useState } from 'react';

function Clock(){
    const [time, setTime] = useState()

    useEffect(() =>{
        const secondsInterval = setInterval(() =>{
            const hour = new Date().getHours().toString().padStart(2,"0");
            const minute = new Date().getMinutes().toString().padStart(2,"0");
            const second = new Date().getSeconds().toString().padStart(2,"0");

            let currentTime = hour + ":" + minute + ":" + second;
            setTime(currentTime);
        }, 1000);

        clearInterval(secondsInterval);
    },[time]);

    return (
        <div>
            <h2>{time}</h2>
        </div>
        
    )
}

export default Clock